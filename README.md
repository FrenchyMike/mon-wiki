## Linux
### [Réseau](https://gitlab.com/FrenchyMike/mon-wiki/-/wikis/Reseau)
* ### [*Fixer l'IP*](https://gitlab.com/FrenchyMike/mon-wiki/-/wikis/Reseau#fixer-lip)
* ### [*Gestion des routes*](https://gitlab.com/FrenchyMike/mon-wiki/-/wikis/Reseau#gestion-des-routes)
* ### [*Configurer un proxy sous ubuntu*](https://gitlab.com/FrenchyMike/mon-wiki/-/wikis/Reseau#configurer-un-proxy-sous-ubuntu)
### [Monitoring de la machine avec cockpit](https://gitlab.com/FrenchyMike/mon-wiki/-/wikis/Cockpit)
### [Montage de partitions logiques](https://gitlab.com/FrenchyMike/mon-wiki/-/wikis/Montage-de-partitions-logiques)
### [Gestion des iptables](https://gitlab.com/FrenchyMike/mon-wiki/-/wikis/Gestion-des-iptables)
### [Script Bash utiles](https://gitlab.com/FrenchyMike/mon-wiki/-/tree/master/script_bash)
### [Apache](https://gitlab.com/FrenchyMike/mon-wiki/-/wikis/apache)

## Docker
#### [Wiki dédié: Home page](https://gitlab.com/FrenchyMike/docker/-/wikis/home)
#### [Premiers pas et actions basiques](https://gitlab.com/FrenchyMike/docker/-/wikis/1.-Premiers-pas)
#### [Commandes utiles](https://gitlab.com/FrenchyMike/docker/-/wikis/2.-Commandes-utiles)
#### [Exemples: trois types d'applications](https://gitlab.com/FrenchyMike/docker/-/wikis/3.-Exemples:-trois-types-d'applications)
* #### [*Exemple d'un conteneur type OS*](https://gitlab.com/FrenchyMike/docker/-/wikis/3.1%20-Conteneur-OS)
* #### [*Exemple sur un conteneur service*](https://gitlab.com/FrenchyMike/docker/-/wikis/3.2-Conteneur-service)
* #### [*Exemple d'un montage de gitlab à l'aide docker compose*](https://gitlab.com/FrenchyMike/docker/-/wikis/3.3-Gitlab-en-local-%C3%A0-l'aide-de-docker-compose)
#### [Les Dockerfile](https://gitlab.com/FrenchyMike/docker/-/wikis/4.-Les-Dockerfiles)
#### [Docker-compose](https://gitlab.com/FrenchyMike/docker/-/wikis/5.-docker-compose)
#### [Docker et le réseau](https://gitlab.com/FrenchyMike/docker/-/wikis/6.-Docker-et-le-r%C3%A9seau)
#### [Connexion à distance](https://gitlab.com/FrenchyMike/docker/-/wikis/7.-Connexion-%C3%A0-distance)
#### [La sécurité dans Docker](https://gitlab.com/FrenchyMike/docker/-/wikis/8.-La-s%C3%A9curit%C3%A9-dans-Docker)
#### [Tag, pull et push](https://gitlab.com/FrenchyMike/docker/-/wikis/9.-Tag,-pull-et-push)
#### [Multistage](https://gitlab.com/FrenchyMike/docker/-/wikis/10.-Multi-stage)
#### [Envoie de logs conteneur dans un ELK](https://gitlab.com/FrenchyMike/docker/-/wikis/11.-Envoie-de-logs-conteneur-vers-ELK)
#### [Best Practice and tips](https://gitlab.com/FrenchyMike/docker/-/wikis/12.-Best-Practises-and-tips)
#### [Annexes](https://gitlab.com/FrenchyMike/docker/-/wikis/99.-Annexes)

## ELK - Elasticsearch, Logstash, Kibana
#### [Installer ELK sur une VM](https://www.digitalocean.com/community/tutorials/how-to-install-elasticsearch-logstash-and-kibana-elastic-stack-on-centos-7)
#### [Configuration locale](https://gitlab.com/FrenchyMike/elk/-/wikis/configuration%20locale)
#### [README.md officielle](https://gitlab.com/FrenchyMike/elk/-/wikis/doc-elk)
#### [Les commandes de bases](https://gitlab.com/FrenchyMike/elk/-/wikis/basic-command)
#### [Exemple de configuration Logstash](https://gitlab.com/FrenchyMike/mon-wiki/-/wikis/Exemple-de-configuration-Logstash)
#### [Docker-compose de la stack](https://gitlab.com/FrenchyMike/docker/-/blob/master/docker-compose/elk/docker-compose.yml)

## Ansible
### [Objectifs](https://gitlab.com/FrenchyMike/mon-wiki/-/wikis/ansible-objectifs)
### [Installation](https://gitlab.com/FrenchyMike/mon-wiki/-/wikis/ansible-installation)
### [Préparer la communication](https://gitlab.com/FrenchyMike/mon-wiki/-/wikis/ansible-communication)
### [Organiser un déploiement](https://gitlab.com/FrenchyMike/mon-wiki/-/wikis/ansible-deploiement)
### [Autres infos](https://gitlab.com/FrenchyMike/mon-wiki/-/wikis/ansible-infos)
### [Exemple d'un rôle complet](https://gitlab.com/FrenchyMike/mon-wiki/-/wikis/ansible-role)

## Kubernetes
### [Commandes utiles](https://gitlab.com/FrenchyMike/mon-wiki/-/wikis/kubernetes-commandes-utiles)

## Micro-service and Service Mesh
### [Envoy Proxy](https://gitlab.com/FrenchyMike/envoy-proxy/-/blob/main/readme.md)

## Activités personnelles
### [Monter un NAS sur un RaspberryPi 4](https://gitlab.com/FrenchyMike/mon-wiki/-/wikis/Monter-un-NAS-sur-un-RaspberryPi-4)
### [Python: Jeu du pendu](https://gitlab.com/FrenchyMike/python_pendu)
### [Python: Jeu du labyrtinthe](https://gitlab.com/FrenchyMike/python_labyrinthe)
### Pipeline de deploiement d'une application Java
* #### [Le fichier de configuration gitlab-ci.yml](https://gitlab.com/FrenchyMike/spring-petclinic-microservices/-/blob/master/.gitlab-ci.yml)
* #### [Exemple de pipeline](https://gitlab.com/FrenchyMike/spring-petclinic-microservices/-/pipelines/213376095)

## Autres documentations utiles
### [YAML Sythaxe](https://learnxinyminutes.com/docs/yaml/)
