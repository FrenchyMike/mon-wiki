# Linter Yml

Idéal pour linter les `.gitlab-ci.yml`  ou encore les `docker-compose.yml`. Il s'agit d'une surcharge de configuration de l'utilitaire yamllint disponible sous Linux.

1. Fichier de configuration

`vim /usr/lib/python3/dist-packages/yamllint/conf/my-conf.yaml`

```yml
---
extends: default
rules:
  line-length:
    max: 8192
  empty-lines:
   max-end: 1
```

2. Créer un alias adapté

`alias ylint="yamllint -c /usr/lib/python3/dist-packages/yamllint/conf/my-conf.yaml"`
