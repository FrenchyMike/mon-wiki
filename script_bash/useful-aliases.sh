#!/bin/bash

echo '# Git
alias gstatus="git status"
alias gdiff="git diff"
alias gpull="git pull"
alias gco="git checkout"

# Docker
alias dps="docker ps"
function dexec(){
    cmd="$2"
    test -n "$1" && docker exec -it "$1" ${cmd:-sh} || echo "Missing container"
}
alias dc="docker-compose"
function dclean(){
    if [ "$1" == "-f" ]; then
        docker rm $(docker ps -q --filter status=exited)
    else
        docker ps --filter status=exited
        echo -e "\n-------------------------------\nRemove these containers ? (Y/n)"
        read ans 
        case $ans in
            Y|y|yes|Yes|YES) 
                docker rm $(docker ps -q --filter status=exited);;
            N|n|no|No|NO) 
                echo "Operation canceled" ;;
            *)  
                echo "flop...";;
        esac
    fi  
}

# Kubernetes
source <(kubectl completion bash)
alias k="kubectl"
alias kcc="kubectl config current-context"
alias kg="kubectl get"
alias kga="kubectl get all --all-namespaces"
alias kgp="kubectl get pods"
alias kgs="kubectl get services"
alias ksgp="kubectl get pods -n kube-system"
alias kuc="kubectl config use-context"
complete -F __start_kubectl k

# System
alias yapt="sudo apt install -y"
function mkcd (){ 
    mkdir "$1"
    cd "$1"
}
alias ..="cd .."
alias ...="cd ../.."
' >> $HOME/.bash_aliases
source $HOME/.bash_aliases
