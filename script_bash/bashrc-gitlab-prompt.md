# Modification du prompt pour Gitlab

A ajouter dans le `$HOME/.bashrc`

```bash
parse_git_branch() {
     git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/ (\1)/'
}

PS1='${debian_chroot:+($debian_chroot)}\[\033[01;44m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\[\e[33m\]$(parse_git_branch)\[\e[m\] \n> \$ '
```

Rendu :
```bash
user@bionic:~/workspace/01_Gitlab/01_DevOps/ansible (master)
> $ 
```
