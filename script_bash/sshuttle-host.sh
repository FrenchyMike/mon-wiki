#!/bin/bash
cd /tmp/
if [ "$1" = "on" ]; then

	echo "activating sshuttle..."
	sudo sshuttle -D -l 0.0.0.0 --dns -r mchevrel@10.107.1.126 0/0 -x 172.17.0.0/24 # -x 127.0.0.1/32

elif [ "$1" = "local" ]; then
	echo "activating shuttle except localhost..."
	sudo sshuttle -D -l 0.0.0.0 --dns -r mchevrel@10.107.1.126 0/0 -x 172.17.0.0/24  -x 127.0.0.1/32

elif [ "$1" = "off" ]; then
	
	ps_sshuttle=$(ps aux | grep "sshuttle -D -l 0.0.0.0" | grep "^root" | awk '{print $2}')
	
	if [ "$ps_sshuttle" = "" ]; then

		echo "pas de processus actif pour sshuttle"

	else

		echo "killing sshuttle on process $ps_sshu"
		sudo killall sshuttle

	fi

elif [ "$1" = "check" ]; then
	
	ps aux |grep "sshuttle"

else

	echo -e "passer un des arguments suivant:\n\t'on' pour activer\n\t'off' pour désactiver\n\t'check' pour vérifier\n pour desactiver"

fi

cd -

exit 0;
