# Commandes et fonctions utiles

Pour avoir le clavier en international (qwerty) :
```bash
function kb(){
    case $1 in
        "intl")
            setxkbmap -layout us -variant intl;;
        "us")
            setxkbmap -layout us;;
        *)
            echo -e "Use:\n\tintl: to set keyboard with international variant\n\tus: to set keyboard on qwerty"
    esac
}
```
