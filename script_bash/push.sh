#!/bin/sh
# Script qui reprend les étapes du push vers gitlab
if [ -f .gitlab-ci.yml ] && [ -f /usr/lib/python3/dist-packages/yamllint/conf/my-conf.yaml ]; then
	yamllint -c /usr/lib/python3/dist-packages/yamllint/conf/my-conf.yaml .gitlab-ci.yml
fi

if [ "$?" != 0 ]; then
	exit 1;
fi

if [ -z "$1" ]; then
	
	IFS= read -r -p "Message de commit: " ans

	if [ -z "$ans" ]; then
		msg="update files"
	else
		msg=$ans
	fi
else
        msg=""
        for word in "$@"
        do
                msg="$msg $word"
        done
fi

git pull
git add .
git commit -am "$msg"
git push
